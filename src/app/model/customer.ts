export class Customer {
    id!: number;
    firstName!: string;
    lastName!: string;
    address!: string;
    city!: string;
    imageUrl!: string;
}

export class SaveCustomer {
    firstName!: string;
    lastName!: string;
    address!: string;
    city!: string;
    imageUrl!: string;
}